const express = require('express');

const {getPedidos,getEstadoPedido,postPedidos,putPedidoUsuario,putPedidoAdmin} = require('../funciones/pedidos.js');

const {validarUsuarioAdmin, validarUsuarioAutenticado} = require('../middlewares/middlewares_autenticacion.js');
const {validarPedidosExistente, validarPedidoCompleto, validarPropiedadPedido, validarEstadoActualizable} = require('../middlewares/middlewares_pedidos.js');

function routerPedidos()
	{
		const router = express.Router();		
		router.get('/pedidos',validarUsuarioAdmin,getPedidos);
		router.get('/pedidos/estado',validarUsuarioAutenticado,validarPedidosExistente,validarPropiedadPedido,getEstadoPedido);
		router.post('/pedidos/crear',validarUsuarioAutenticado,validarPedidoCompleto,postPedidos);
		router.put('/pedidos/actualizar/',validarUsuarioAutenticado,validarPedidosExistente,validarPropiedadPedido,validarEstadoActualizable,putPedidoUsuario);
		router.put('/pedidos/actualizar/estado',validarUsuarioAdmin,validarPedidosExistente,putPedidoAdmin);
		return router;
	}

module.exports={routerPedidos};