const express = require('express');

const {getMetodosPagos, postMetodosPagos, putMetodosPagos, deleteMetodosPagos} = require('../funciones/pagos.js');
const {validarPagoRegistrado, validarPagoExistente} = require('../middlewares/middlewares_pagos.js');
const {validarUsuarioAdmin, validarUsuarioAutenticado} = require('../middlewares/middlewares_autenticacion.js');

function routerPagos()
	{
		const router = express.Router();		
		router.get('/pagos',validarUsuarioAutenticado,getMetodosPagos);
		router.post('/pagos/crear',validarUsuarioAdmin,validarPagoRegistrado,postMetodosPagos);
		router.put('/pagos/editar',validarUsuarioAdmin,validarPagoExistente,putMetodosPagos);
		router.delete('/pagos/eliminar',validarUsuarioAdmin,validarPagoExistente,deleteMetodosPagos);
		return router
	}

module.exports={routerPagos};