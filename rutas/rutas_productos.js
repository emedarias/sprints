const express = require('express');

const {getProductos, postProducto, putProducto, deleteProducto} = require('../funciones/productos.js');
const {validarProductoRegistrado, validarProductoExistente} = require('../middlewares/middlewares_productos.js');
const {validarUsuarioAdmin, validarUsuarioAutenticado} = require('../middlewares/middlewares_autenticacion.js');

function routerProductos()
	{
		const router = express.Router();		
		router.get('/productos',validarUsuarioAutenticado,getProductos);
		router.post('/productos/crear',validarUsuarioAdmin,validarProductoRegistrado,postProducto);
		router.put('/productos/editar',validarUsuarioAdmin,validarProductoExistente,putProducto);
		router.delete('/productos/eliminar',validarUsuarioAdmin,validarProductoExistente,deleteProducto);
		return router
	}

module.exports={routerProductos};