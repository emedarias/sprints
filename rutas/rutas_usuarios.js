const express = require('express');

const {getUsuarios, postUsuario} = require('../funciones/usuarios.js');
const {validarMailExistente, validarPass} = require('../middlewares/middlewares_usuarios.js');
const {validarUsuarioAdmin} = require('../middlewares/middlewares_autenticacion.js');

function routerUsuarios()
	{
		const router = express.Router();		
		router.get('/usuarios',validarUsuarioAdmin,getUsuarios);
		router.post('/usuarios/crear',validarPass,validarMailExistente,postUsuario);
		return router
	}

module.exports={routerUsuarios};