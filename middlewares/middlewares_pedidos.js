const {pedidos, metodosPago, estadoPedidos, productos} = require("../datos/datos.js");

//Funcion para validar si el mail esta registrado.
function validarPedidosExistente(req, res, next)
	{
		for(const pedido of pedidos)
			{
				if(pedido.id === Number(req.body.id))
					{
						return next();				
					}
			}
		return res.status(404).send("El pedido no esta registrado.");
	};

function validarPedidoCompleto(req, res, next)
	{
		
		if(req.body.pagoId != "")
			{
				if(req.body.productos!=[])
					{
						return next();
					}
				else
					{
						return res.status(406).send("El pedido no tiene productos agregados.");			
					}
			}
		else
			{
				return res.status(406).send("El pedido no tiene metodo de pago agregado.");
			}							
	};

function validarPropiedadPedido(req, res, next)
	{
		
		for(const pedido of pedidos)
			{
				if(pedido.mail === req.headers.mail)
					{
						return next();	
					}		
			}

		return res.status(403).send("El usuario no es el propietario del pedido.");							
	};

function validarEstadoActualizable(req, res, next)
	{
		for(const pedido of pedidos)
			{
				if(pedido.id === Number(req.body.id))
					{
						if(pedido.estadoId===1)
							{
								return next();						
							}
					}
			}
		return res.status(400).send("El pedido ya no es actualizable.");
	}

module.exports={validarPedidosExistente, validarPedidoCompleto, validarPropiedadPedido, validarEstadoActualizable}