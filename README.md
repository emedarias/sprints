# [Sprint01 - Delilah Restaurant](https://gitlab.com/emedarias/sprint01)

### Descripción

Este es el primer sprint del curso de desarrollo web backend. En este API de un restaurante llamado Delilah, se podran crear, eliminar, actualizar y ver los pedidos, productos, usuarios y medios de pagos del restaurante.


### Instalación

Para instalar y correr la API se deben utilizar los siguientes comandos:
```
$ npm install
$ npm run start
```

### Tecnologias

* [NodeJS](https://nodejs.org/en): Version 16.2.0
* [Express](https://expressjs.com/es): Version 4.17.1
* [Swagger-UI-express](https://www.npmjs.com/package/swagger-ui-express): Version 4.1.6

### Autor

Arias Emilio Eduardo