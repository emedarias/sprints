const {usuarios, autenticados} = require("../datos/datos.js");

//funcion para ver productos. Debe sacarse en la produccion.
function login(req, res)
	{
		for(const usuario in usuarios)
			{
				if(usuarios[usuario].mail === req.body.mail && usuarios[usuario].password === req.body.password)
					{
						const id = Number(usuario);
						
						if(autenticados.includes(id))
							{
								return res.send("El usuario ya esta autenticado.");
							}
						else
							{
								autenticados.push(id);
								return res.status(200).send("Bienvenido "+usuarios[usuario].nombre+".");		
							}
					}
			}
		return res.status(400).send("El password no es correcto.");
	}

module.exports={login};