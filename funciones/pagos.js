const {metodosPagos} = require("../datos/datos.js");

//funcion para ver metodosPagos. Debe sacarse en la produccion.
function getMetodosPagos(req, res)
	{
		if(metodosPagos)
			{
				return res.status(200).send(metodosPagos);
			}
		return res.status(404).send("No hay metodos pagos registrados.");
	}

//funcion para crear metodosPagos
function postMetodosPagos(req, res)
	{
		let id;
		if(metodosPagos.length>0)
			{
				id=metodosPagos[metodosPagos.length-1].id+1;
			}
		else
			{
				id=0;
			}
		
		const metodo = req.body.metodo;
		
		const nuevoMetodosPagos={id, metodo};
		
		metodosPagos.push(nuevoMetodosPagos);
		return res.status(201).send("El metodo de pago fue creado con exito.");		
	}

//funcion para editar metodosPagos
function putMetodosPagos(req, res)
	{
		for(const metodo in metodosPagos)
			{
				if(metodosPagos[metodo].id === Number(req.body.id))
					{
						metodosPagos[metodo].metodo=req.body.metodo;
						return res.status(201).send("El metodo de pago fue editado con exito.");	
					}
			}
	}

//funcion para eliminar metodosPagos
function deleteMetodosPagos(req, res)
	{
		let eliminar;

		for(const metodo in metodosPagos)
			{
				if(metodosPagos[metodo].id === Number(req.body.id))
					{
						 eliminar = Number(metodo);
					}
			}

		metodosPagos.splice(eliminar,1);
		return res.status(201).send("El metodo de pago fue eliminado con exito.");			
	}

module.exports={getMetodosPagos, postMetodosPagos, putMetodosPagos, deleteMetodosPagos};