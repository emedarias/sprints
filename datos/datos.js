const usuarios = [{"id":1, "nombre":"Emilio", "direccion":"calle falsa 123" , "mail":"emiliorecapo@gmail.com", "telefono":"2964525540", "password":"lalo123", "admin": true}, 
					{"id":2, "nombre":"Eduardo", "direccion":"calle falsa 123" , "mail":"emilionotancapo@gmail.com", "telefono":"2964525540", "password":"lalo123", "admin": false}
				];

const autenticados = [];

const metodosPagos = [{"id":1, "metodo":"efectivo"}];

const estadoPedidos = [{"id":1, "estado":"Pendiente"},
						{"id":2, "estado":"Confirmado"},
						{"id":3, "estado":"En preparacion"},
						{"id":4, "estado":"Enviado"},
						{"id":5, "estado":"Entregado"},
						{"id":6, "estado":"Cancelado"}
					];

const pedidos =[{
        "id": 1627670544308,
        "estadoId": 1,
        "pagoId": 1,
        "direccion": "calle falsa 123",
        "telefono": "2964525540",
        "mail": "emiliorecapo@gmail.com",
        "productosPedido": [
            {
                "nombre": "pizza",
                "precio": 500,
                "cantidad": 2
            },
            {
                "nombre": "hamburguesa",
                "precio": 400,
                "cantidad": 1
            }
        ],
        "totalPagar": 1400
    }]

const productos=[{"id":1, "nombre":"pizza", "precio":500},
					{"id":2, "nombre":"hamburguesa", "precio":400},
					{"id":3, "nombre":"gaseosa", "precio":250}]
					
module.exports={usuarios,autenticados,estadoPedidos,productos,metodosPagos, pedidos};